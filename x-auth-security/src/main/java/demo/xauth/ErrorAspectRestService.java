package demo.xauth;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@EnableAspectJAutoProxy(proxyTargetClass = true)
@Aspect
@Component
public class ErrorAspectRestService {

    public ErrorAspectRestService() {
    }

    // @Bean
    // @AfterThrowing(pointcut = "execution(* com.smartinnotec.aposoft.security.xauth.*.doFilter(..))", throwing = "e")
    // @Before("doFilter(request)")
    // @Pointcut("execution(* com.smartinnotec.aposoft.security.xauth.XAuthTokenFilter.doFilter(ServletRequest, ServletResponse, FilterChain))")
    // @After("doFilter(*)")
    // @Pointcut("execution(public * *(..))")
    // @Before("com.smartinnotec.aposoft.security.xauth.XAuthTokenFilter.testMethod()")
    // @Around("testMethod()")
    // @AfterReturning("execution(* com..*Filter.*(..))")
    // @Pointcut("execution(* com.smartinnotec.aposoft.security.xauth.XAuthTokenFilter.doFilter(..))")
    @Before("execution(* demo.xauth.XAuthTokenFilter.doFilter(..))")
    public void logServiceAccess() {
        System.out.println("ddddddddddddddddddddddddddddddddddddddd ----------------------- Completed: ");
    }
}
