package demo.xauth;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

/**
 * Sifts through all incoming requests and installs a Spring Security principal if a header corresponding to a valid
 * user is found.
 *
 * @author Philip W. Sorst (philip@sorst.net)
 * @author Josh Long (josh@joshlong.com)
 */

@EnableAspectJAutoProxy(proxyTargetClass = true)
@Component
public class XAuthTokenFilter extends GenericFilterBean {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private UserDetailsService detailsService;
    private final TokenUtils tokenUtils = new TokenUtils();
    private String xAuthTokenHeaderName; // configured in application.properties

    public XAuthTokenFilter() {
    }

    public UserDetailsService getDetailsService() {
        return detailsService;
    }

    public void setDetailsService(final UserDetailsService detailsService) {
        this.detailsService = detailsService;
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {
        testMethod();
        try {
            final HttpServletRequest httpServletRequest = (HttpServletRequest)request;
            final String authToken = httpServletRequest.getHeader("x-auth-token");

            logger.info("filter request for token '{}' in XAuthTokenFilter#doFilter", authToken);

            if (StringUtils.hasText(authToken)) {
                final String username = this.tokenUtils.getUserNameFromToken(authToken);

                final UserDetails details = this.detailsService.loadUserByUsername(username);

                final boolean isTokenValid = this.tokenUtils.validateToken(authToken, details);
                logger.info("token is valid = '{}' in XAuthTokenFilter#doFilter for user with user name '{}'", isTokenValid, username);
                if (isTokenValid) {
                    final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(details,
                        details.getPassword(), details.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(token);
                } else {
                    logger.info("token '{}' is not valid for user '{}' in AuthTokenFilter", authToken, username);
                }
            }
            filterChain.doFilter(request, response);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public String getxAuthTokenHeaderName() {
        return xAuthTokenHeaderName;
    }

    public void setxAuthTokenHeaderName(final String xAuthTokenHeaderName) {
        this.xAuthTokenHeaderName = xAuthTokenHeaderName;
    }

    public String testMethod() {
        return "TestMethod !!!";
    }

    @Override
    public String toString() {
        return "[XAuthTokenFilter: xAuthTokenHeaderName: " + xAuthTokenHeaderName + "]";
    }
}
